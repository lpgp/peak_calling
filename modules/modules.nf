#!/usr/bin/env nextflow

// COVERAGE
process COVERAGE {
	tag "${replicateId}"

	publishDir "${params.out_dir}/coverage", mode: 'copy', overwrite: true
	
	input:
	tuple val(replicateId), path(bam)

	output:
	path '*.coverage'

	script:
	"""
	conda run -n env samtools \
		coverage \
		-o "${replicateId}.coverage" \
		${bam[0]}

	"""
}

// DEPTH
process DEPTH {
	tag "${replicateId}"

	publishDir "${params.out_dir}/depth", mode: 'copy', overwrite: true
	
	input:
	tuple val(replicateId), path(bam)

	output:
	path '*.depth'

	script:
	"""
	conda run -n env samtools \
		depth \
		-o tmp.txt \
		${bam[0]}

	awk '{print \$3}' tmp.txt | sort | uniq -c > "${replicateId}.depth" 
	"""
}

// PLOTFINGERPRINT
process PLOTFINGERPRINT {
	tag "${replicateId}"

	publishDir "${params.out_dir}/plotfingerprint", mode: 'copy', overwrite: true
	
	input:
	tuple val(replicateId), path(bam)

	output:
	path '*.{txt,png}'

	script:
	def bamfiles = params.ATAC ? "--bamfiles  ${bam[0]}" : "--bamfiles  ${bam[0]} ${bam[2]}"

	"""
	conda run -n deeptools plotFingerprint \
        ${bamfiles} \
        --plotFile ${replicateId}.plotFingerprint.png \
        --outRawCounts  ${replicateId}.plotFingerprint.raw.txt \
        --outQualityMetrics  ${replicateId}.plotFingerprint.qcmetrics.txt \
        --skipZeros \
        --numberOfProcessors "max" \
		--numberOfSamples ${params.numberOfSamples}	
	"""
}

// MACS
process MACS {

	tag "${replicateId}"
	
	input:
	tuple val(replicateId), path(bam)

	output:
	path "MACS/${replicateId}_peaks.{broadPeak,narrowPeak}", emit: peaks
	path "${bam[0]}", emit: bam

	script:
    def control = params.ATAC ? "" : "-c ${bam[2]}"
	def type = params.broad ? "--broad --broad-cutoff ${params.broad_cutoff}" : ""
	def nomodel = params.ATAC ? "--nomodel" : ""
	def flag = params.broad ? "broadPeak" : "narrowPeak"

	"""
	conda run -n macs3 macs3 callpeak \
		-t ${bam[0]} \
		${control} \
		-n ${replicateId} \
		-f BAMPE \
		-g ${params.gsize} \
        -q ${params.qvalue} \
		${type} \
		--outdir MACS \
		--keep-dup all \
        ${nomodel}

	conda run -n env samtools \
		flagstat \
		${bam[0]} \
		> "${replicateId}.flagstats"

	cat ./MACS/${replicateId}_peaks.${flag} | wc -l | awk -v OFS='\t' '{ print "${replicateId}", \$1 }' > ./MACS/${replicateId}_peaks.count_mqc.tsv 
    READS_IN_PEAKS=\$(conda run -n env intersectBed -a ${bam[0]} -b ./MACS/${replicateId}_peaks.${flag} -bed -c -f 0.20 | awk -F '\t' '{sum += \$NF} END {print sum}')
    grep 'mapped (' "${replicateId}.flagstats" | awk -v a="\$READS_IN_PEAKS" -v OFS='\t' '{print "${replicateId}", a/\$1}' > ./MACS/${replicateId}_peaks.FRiP_mqc.tsv
	
	cp -r MACS "${params.out_dir}"
	"""
}

// Consensus peaks and count
process COUNT {

	tag "all samples"

    publishDir "${params.out_dir}/count", mode: 'copy', overwrite: true

	input:
	path peaks
	path bam

	output:
	path 'consensus.featureCounts.txt'

	script:
    def mergecols = params.broad ? (2..9).join(',') : (2..10).join(',')
	def collapsecols = params.broad ?  (['collapse']*8).join(',') : (['collapse']*9).join(',') 
    def expandparam = params.broad ?  '' : '--is_narrow_peak'

    """
	cat ${peaks.sort().join(' ')} > combined.narrowPeak

	sort -T '.' -k1,1 -k2,2n combined.narrowPeak > sorted_combined.narrowPeak

	conda run -n env mergeBed \
	-i sorted_combined.narrowPeak \
	-c ${mergecols} \
	-o ${collapsecols} > consensus.txt

	awk 'NF >= 3' consensus.txt > consensus_cleaned.txt

	conda run -n macs3 python "$baseDir/bin/macs_merged_expand.py" \
		consensus_cleaned.txt \
        ${peaks.sort().join(',').replaceAll("_peaks.*","")} \
        consensus.boolean.txt \
        --min_replicates ${params.min_reps_consensus} \
        ${expandparam}

    awk -v FS='\t' -v OFS='\t' 'FNR > 1 { print \$1, \$2, \$3, \$4, "0", "+" }' consensus.boolean.txt > consensus.bed

    echo -e "GeneID\tChr\tStart\tEnd\tStrand" > consensus.saf
    awk -v FS='\t' -v OFS='\t' 'FNR > 1 { print \$4, \$1, \$2, \$3,  "+" }' consensus.boolean.txt >> consensus.saf

    conda run -n env featureCounts \
        -F SAF \
        -O \
        --fracOverlap 0.2 \
        -T 2 \
        -p --donotsort \
        -a consensus.saf \
        -o consensus.featureCounts.txt \
        ${bam}
	"""
}
