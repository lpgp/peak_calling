#!/usr/bin/env nextflow

// Enable DSL 2 syntax
nextflow.enable.dsl = 2

// Import modules 
include {
	DEPTH;
	COVERAGE;
	PLOTFINGERPRINT;
	MACS;
	COUNT
} from "./modules/modules.nf"

// output directory
file("${params.out_dir}").mkdirs()

// main pipeline logic
workflow {

	// CHIP input
	if(params.ATAC == false){

		bam=Channel.fromPath(params.input)
			.splitCsv(header:true, sep:',')
    		.map {
        		row -> tuple(
            		row.ID,
            		[
						file(row.target,checkIfExists: true),
						file(row.target.replaceFirst(/\.bam$/,".bam.bai"),checkIfExists: true),
						file(row.input,checkIfExists: true),
						file(row.input.replaceFirst(/\.bam$/,".bam.bai"),checkIfExists: true)
					]
				)
    		}
	}else{

		bam=Channel.fromPath(params.input)
			.splitCsv(header:true, sep:',')
    		.map {
        		row -> tuple(
            		row.ID,
            		[
						file(row.target,checkIfExists: true),
						file(row.target.replaceFirst(/\.bam$/,".bam.bai"),checkIfExists: true)
					]
        		)
    		}
	}

	// coverage
    if(params.skip_depth == false){
	    DEPTH(
	    	bam
	    )
    }

	// DEPTH
    if(params.skip_coverage == false){
	    COVERAGE(
	    	bam
	    )
    }

	// plotfingerprint
    if(params.skip_plotfingerprint == false){
	    PLOTFINGERPRINT(
	    	bam
	    )
    }

	// peak calling and read count
	if(params.skip_macs == false){
		MACS(
			bam
		)

		COUNT(
			MACS.out.peaks.collect(),
			MACS.out.bam.collect()
		)
	}
}